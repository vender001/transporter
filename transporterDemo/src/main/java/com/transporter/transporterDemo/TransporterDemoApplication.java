package com.transporter.transporterDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransporterDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransporterDemoApplication.class, args);
	}

}
